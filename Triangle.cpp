/*
 * Triangle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#include "Triangle.h"

Triangle::Triangle(double a, double b, double c) {
	setA(a);
	setB(b);
	setC(c);
}

Triangle::~Triangle() {

}

void Triangle::setA(double a){
	Triangle::a = a;
}

void Triangle::setB(double b){
	Triangle::b = b;
}

void Triangle::setC(double c){
	Triangle::c = c;
}

double Triangle::calculateCircumference(){
	return Triangle::a + Triangle::b + Triangle::c;
}

