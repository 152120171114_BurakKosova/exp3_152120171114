/*
 * Circle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */


#include "Circle.h"

const double Circle::PI = 3.14;

Circle::Circle(double r) {
	setR(r);
}

Circle::~Circle() {
}

void Circle::setR(double r){
	this->r = r;
}

void Circle::setR(int r){
	this->r = r;
}

double Circle::getR() const{
	return Circle::r;
}

double Circle::calculateCircumference()const {
	return Circle::PI * Circle::r * 2;
}

double Circle::calculateArea(){
	return PI * Circle::r * Circle::r;
}

bool Circle::equals(Circle c) {
	return c.getR() == Circle::r;
}

